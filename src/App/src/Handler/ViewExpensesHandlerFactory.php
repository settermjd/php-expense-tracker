<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;

use function assert;

class ViewExpensesHandlerFactory
{
    public function __invoke(ContainerInterface $container): ViewExpensesHandler
    {
        if (! $container->has(TemplateRendererInterface::class)) {
            throw new ServiceNotFoundException("Template provider service is not available in the container");
        }

        $template = $container->get(TemplateRendererInterface::class);
        assert($template instanceof TemplateRendererInterface);

        return new ViewExpensesHandler($template);
    }
}
