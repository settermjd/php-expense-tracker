<?php

declare(strict_types=1);

namespace App\Entity\Doctrine;

use \Money\{Currency, Money};
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class DoctrineMoney
{
    #[ORM\Column(type: Types::INTEGER)]
    private int|string $amount;

    #[ORM\Column(type: Types::STRING)]
    private string $currencyCode;

    public function getValue(): Money
    {
        return new Money($this->amount, new Currency($this->currencyCode));
    }

    public function setValue(Money $money)
    {
        $this->amount = $money->getAmount();
        $this->currencyCode = $money->getCurrency()->getCode();
    }
}
