<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Doctrine\DoctrineMoney;
use App\Repository\ExpenseRepository;
use DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ExpenseRepository::class)]
#[ORM\Table(name: 'expense')]
#[ORM\Index(columns: ['amount', 'location', 'date', 'description'], name: 'expense_idx')]
class Expense
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(name: 'id', type: Types::INTEGER, unique: true, nullable: false)]
    protected int|null $id;

    #[ORM\Column(name: 'description', type: Types::TEXT, unique: false, nullable: true)]
    private ?string $description;

    /**
     * @var App\Entity\Doctrine\DoctrineMoney
     */
    #[ORM\Column(name: 'amount', type: Types::STRING, unique: false, nullable: false)]
    #[ORM\Embedded(class: DoctrineMoney::class)]
    private $amount;

    #[ORM\Column(name: 'location', type: Types::STRING, unique: false, nullable: false)]
    private string $location;

    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, unique: false, nullable: false)]
    private \DateTime $date;

    #[ORM\Column(name: 'receipt', type: Types::BLOB, unique: false, nullable: true)]
    private string $receipt;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'expenses')]
    #[ORM\JoinColumn(name: 'category_id', referencedColumnName: 'id')]
    private ?Category $category = null;

    public function __construct(
        ?DateTime $date,
        ?DoctrineMoney   $amount,
        ?string   $description,
        ?string   $location,
        ?string   $receipt,
        ?int      $id = null,
        ?Category $category = null,
    ) {
        $this->amount->setValue($amount);
        $this->category = $category;
        $this->date = $date;
        $this->description = $description;
        $this->id   = $id;
        $this->location = $location;
        $this->receipt = $receipt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function getAmount()
    {
        return $this->amount->getValue();
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function getReceipt(): ?string
    {
        return $this->receipt;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function __toArray(): array
    {
        return [
            'id'          => $this->id,
            'amount'      => $this->amount,
            'description' => $this->description,
            'date'        => $this->date,
            'category'    => $this->category,
            'location'    => $this->location,
            'receipt'     => $this->receipt,
        ];
    }
}
