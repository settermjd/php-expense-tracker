<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ORM\Table(name: 'category')]
#[ORM\Index(columns: ['category'], name: 'category_idx')]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(name: 'id', type: Types::INTEGER, unique: true, nullable: false)]
    protected int|null $id;

    #[ORM\Column(name: 'category', type: Types::STRING, unique: true, nullable: true)]
    private $category;

    /**
     * @var ArrayCollection<int, Expense>
     */
    #[ORM\OneToMany(targetEntity: Expense::class, mappedBy: 'category')]
    private ArrayCollection $expenses;

    public function __construct(
        $category,
        ?int $id = null
    ) {
        $this->id   = $id;
        $this->category = $category;
        $this->expenses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function getExpenses(): ArrayCollection
    {
        return $this->expenses;
    }

    public function __toArray(): array
    {
        return [
            'id'       => $this->getId(),
            'category' => $this->getCategory(),
        ];
    }
}
