<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Category;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class CategoryRepositoryFactory
{
    public function __invoke(ContainerInterface $container): CategoryRepository
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        return $entityManager->getRepository(Category::class);
    }
}
