<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Expense;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class ExpenseRepositoryFactory
{
    public function __invoke(ContainerInterface $container): ExpenseRepository
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        return $entityManager->getRepository(Expense::class);
    }
}
