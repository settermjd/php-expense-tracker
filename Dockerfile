# syntax = docker/dockerfile:1

# Set the version of Debian to use
ARG DEBIAN_VERSION=bookworm

# Set the version of PHP to use
# I'd like it to be 8.3, but the MailChimp library makes that almost impossible
# as it uses dynamic properties
ARG PHP_VERSION=8.2


# The base staged used by all future stages throughout the file
FROM php:${PHP_VERSION}-apache-${DEBIAN_VERSION} as base

ARG APACHE_RUN_USER=www-data
ARG APACHE_RUN_GROUP=www-data
ARG APACHE_DOCUMENT_ROOT=/var/www

# Set the release of the official Docker Hub Composer image to use
ARG COMPOSER_VERSION=2.7.7

# Install the required PHP extensions
RUN docker-php-ext-install bcmath

RUN chown -Rv ${APACHE_RUN_USER}:${APACHE_RUN_GROUP} ${APACHE_DOCUMENT_ROOT}

# Install Composer as it's required for installing extensions
COPY --from=composer:2.7.7 \
    --chmod=+x \
    /usr/bin/composer \
    /usr/bin/

# Install the dependencies required for Node and Composer
RUN apt-get update \
    && apt install -y \
    curl \
    git \
    jq \
    libzip-dev \
    && docker-php-ext-install zip

#
# The dependencies stage handles installing all of the userland dependencies,
# such as Composer and installing the PHP dependencies.
FROM base as dependencies

ARG APACHE_RUN_USER=www-data
ARG APACHE_RUN_GROUP=www-data

# Set where to tell Apache to look for the application's document root directory
ARG APACHE_DOCUMENT_ROOT=/var/www

# Set the path to Composer
ARG COMPOSER_HOME=/usr/bin

# Sets whether Composer should install packages quietly (with no output) or not.
# Set this to "n" if you need to debug this step.
ARG COMPOSER_QUIET_INSTALL="y"

# The PHP app lives here
WORKDIR ${APACHE_DOCUMENT_ROOT}

# Copy over the application code
COPY . .

# Install Node.js and build a minified version of the application's stylesheet
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash \
    && exec bash \
    && nvm install --lts \
    && npx tailwindcss -i ./src/assets/css/styles.css -o ./public/css/styles-min.css --minify \
    && rm -rvf /var/cache/apt/archives

# Run composer audit to check for package vulnerabilities and install PHP's dependencies
RUN bin/install-php-extensions.sh

# Should never run Composer as root
# Should set this user from an environment variable or build argument
USER ${APACHE_RUN_USER}

# Create the Composer cache directory and make it writable, as the www-user does
# not have a home directory by default
RUN mkdir -p ${APACHE_DOCUMENT_ROOT}/.composer/cache/repo/https---repo.packagist.org

RUN bin/install-php-dependencies.sh

# The final stage contains only enough to run the app, with no unnecessary bloat
FROM base as final

ARG DATA_DIR=/opt/data
ARG APACHE_DOCUMENT_ROOT=/var/www

# The PHP app lives here
WORKDIR ${APACHE_DOCUMENT_ROOT}

# Copy over the application code
COPY . .

# Copy over the Composer vendor directory from the dependencies stage
COPY --from=dependencies ${APACHE_DOCUMENT_ROOT}/vendor ./vendor

# Change the document root and enable mod_rewrite
RUN sed -ri -e "s!/var/www/html!${APACHE_DOCUMENT_ROOT}/public!g" /etc/apache2/sites-available/*.conf \
    && sed -ri -e "s!/var/www/!${APACHE_DOCUMENT_ROOT}/public!g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf \
    && a2enmod rewrite

# Build the data directory structure and ensure that they can be written in
RUN mkdir -p "${APACHE_DOCUMENT_ROOT}/logs" "${APACHE_DOCUMENT_ROOT}/cache" \
    && chown -Rv ${APACHE_RUN_USER}:${APACHE_RUN_GROUP} "${APACHE_DOCUMENT_ROOT}" \
    && chmod -Rv ug+w "${APACHE_DOCUMENT_ROOT}"

# Remove any lingering cache file
#RUN composer clear-config-cache