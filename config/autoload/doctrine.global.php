<?php

declare(strict_types=1);

use Doctrine\DBAL\Driver\PDO\SQLite\Driver;
use Doctrine\ORM\Mapping\Driver\AttributeDriver;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driver_class'         => Driver::class,
                'params'               => [
                    'path' => __DIR__ . "/../../data/database.sqlite3",
                ],
            ],
        ],
        'driver'     => [
            'orm_default' => [
                'drivers' => [
                    'App\Entity' => [
                        'class' => AttributeDriver::class,
                        'cache' => 'array',
                        'paths' => [
                            __DIR__ . '/../../src/App/src/Entity',
                        ],
                    ],
                ],
            ],
        ],
        'configuration' => [
            'orm_default' => [
                'types' => [],
            ],
        ],
        'migrations' => [
            'orm_default' => [
                'migrations_paths' => [
                    'Skar\LaminasDoctrineORM' => __DIR__ . '/../../database/migrations',
                ]
            ],
        ],
        'orm' => [
            'mappings' => [
                'Money' => [
                    'type' => 'yml',
                    'dir' => __DIR__ . '/../../config/doctrine/resources/Money',
                    'prefix' => 'Money',
                ]
            ]
        ]
    ],
];
