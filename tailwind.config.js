/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.html"],
  theme: {
    extend: {
      backgroundImage: {
        'currency-pattern': "url('/static/img/icons/finance-symbols.png')"
      }
    },
  },
  plugins: [],
}

