# PHP Expense Tracker

This is a small PHP-based expense tracker application. 

It's not meant to be a full-featured application, one that can compete with either open source or closed source projects, such as [YNAB][ynab-url], [Ivy Wallet][ivy-wallet-url], [Firefly][firefly-url], [GnuCash][gnucash-url], or [Pennyworth][pennyworth-url]. 

The intent of the project is to be an application that supports a tutorial and video series I'm working on, which shows how to design, build, test, and deploy a PHP-based web application from start to finish.

So, don't be disappointed if it's pretty light on in features.

## Prerequisites

- Docker Compose
- Familiarity with PHP and the terminal/command-line

## Getting Started

After cloning the source, you can start the application by running the following command:

```bash
docker compose up -d
```

The first time you run the command, it will take a little while to start, as the base image needs to be downloaded to your local Docker image cache.
Anytime thereafter, it should start quite quickly.

After the application's up and running, you can then view it at http://localhost:8080.

## Enable application development mode

The application comes with [laminas-development-mode](https://github.com/laminas/laminas-development-mode), which provides a Composer script that allows you to enable and disable development mode.

### To enable development mode

```bash
composer development-enable
```

**Note:** Enabling development mode will also clear your configuration cache, to
allow safely updating dependencies and ensuring any new configuration is picked
up by your application.

### To disable development mode

```bash
composer development-disable
```

### View development mode status

```bash
composer development-status
```

## Configuration caching

By default, the skeleton will create a configuration cache in
`data/config-cache.php`. When in development mode, the configuration cache is
disabled, and switching in and out of development mode will remove the
configuration cache.

You may need to clear the configuration cache in production when deploying if
you deploy to the same directory. You may do so using the following:

```bash
composer clear-config-cache
```

You may also change the location of the configuration cache itself by editing
the `config/config.php` file and changing the `config_cache_path` entry of the
local `$cacheConfig` variable.

[ynab-url]: https://www.ynab.com/
[ivy-wallet-url]: https://ivywallet.app/
[firefly-url]: https://www.firefly-iii.org/
[gnucash-url]: https://gnucash.org/
[pennyworth-url]: https://apps.apple.com/au/app/pennyworth-expense-tracker-app/id954944283