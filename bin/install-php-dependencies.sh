#!/bin/sh

if [ -z ${COMPOSER_HOME} ]; then
    echo "Path to Composer was not set"
    exit 1
fi

# Install PHP's dependencies
if [ "${COMPOSER_QUIET_INSTALL}" = "y" ]; then
    "${COMPOSER_HOME}/composer" install \
        --classmap-authoritative \
        --no-ansi \
        --no-dev \
        --no-interaction \
        --no-plugins \
        --no-progress \
        --no-scripts \
        --quiet
else 
    "${COMPOSER_HOME}/composer" install \
        --classmap-authoritative \
        --no-ansi \
        --no-dev \
        --no-interaction \
        --no-plugins \
        --no-progress \
        --no-scripts
fi