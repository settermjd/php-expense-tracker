#!/bin/bash

set -o posix
set -eu 

# This is a small shell script to install all PHP extensions listed in a
# project's composer.json configuration file, with the assumption that it's
# being run in a Dockerfile.

VERBOSE=true

# Process the options passed to the script
# The script only supports two options at the moment: 
# - v: for verbose output 
# - f: for the full path to composer.json
while getopts "q" options; do
    case "${options}" in
        q)
            VERBOSE=false
        ;;
        *)
            exit_abnormal
        ;;
    esac
done


# This function installs the missing extensions, along with any packages which
# those extensions require
function install_missing_extensions()
{
    missing_extensions=$(composer check-platform-reqs --format json 2>/dev/null | jq --raw-output '.[] | select( [ .status | contains("missing") ] | any ) | .name' | grep -oP "(?<=ext-)[a-yA-Y0-9]*$")
    missing_extensions=($missing_extensions)

    if (( ${#missing_extensions[@]} > 0 )) 
    then
        $VERBOSE && echo -e "\nChecking if extra packages are required before installing missing extensions" 
        for extension in ${missing_extensions[@]}
        do 
            case ${extension} in 
            "gmp")
                required_libs+=("libgmp-dev")
                ;;
            "intl")
                required_libs+=("libicu-dev")
                ;;
            "xml")
                required_libs+=("libxml2-dev")
                ;;
            esac
        done

        apt-get update

        if (( ${#required_libs[@]} > 0 ))
        then
            $VERBOSE && echo -e "\nInstalling the required packages\n"
            apt-get install -y ${required_libs}
        fi

        $VERBOSE && echo -e "\nInstalling the required extensions\n"
        docker-php-ext-install ${missing_extensions}

        $VERBOSE && echo -e "\nMissing extensions have been installed.\n"
    fi
}

function usage() {
    echo "Usage: $0 [ -v ] [ -f Path to composer.json ]" 1>&2 
}

function exit_abnormal() {
    usage   
    exit 1
}

install_missing_extensions