<?php

declare(strict_types=1);

namespace AppTest\Handler;

use App\Handler\ViewExpensesHandler;
use App\Handler\ViewExpensesHandlerFactory;
use AppTest\InMemoryContainer;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Mezzio\Router\RouterInterface;
use Mezzio\Template\TemplateRendererInterface;
use PHPUnit\Framework\TestCase;

class ViewExpensesHandlerFactoryTest extends TestCase
{
    public function testFactoryWithTemplate(): void
    {
        $container = new InMemoryContainer();
        $container->setService(RouterInterface::class, $this->createMock(RouterInterface::class));
        $container->setService(TemplateRendererInterface::class, $this->createMock(TemplateRendererInterface::class));

        $factory  = new ViewExpensesHandlerFactory();
        $homePage = $factory($container);

        self::assertInstanceOf(ViewExpensesHandler::class, $homePage);
    }

    public function testThrowsExceptionIfTemplateNotProvided(): void
    {
        $this->expectException(ServiceNotFoundException::class);
        $this->expectExceptionMessage("Template provider service is not available in the container");

        $container = new InMemoryContainer();

        $factory  = new ViewExpensesHandlerFactory();
        $homePage = $factory($container);
    }
}