<?php

declare(strict_types=1);

namespace AppTest\Handler;

use App\Handler\ViewExpensesHandler;
use Laminas\Diactoros\Response\HtmlResponse;
use Mezzio\Template\TemplateRendererInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

class ViewExpensesHandlerTest extends TestCase
{
    public function testReturnsHtmlResponse(): void
    {
        $renderer = $this->createMock(TemplateRendererInterface::class);
        $renderer
            ->expects($this->once())
            ->method('render')
            ->with('app::view-expenses', $this->isType('array'))
            ->willReturn('');

        $homePage = new ViewExpensesHandler($renderer);

        $response = $homePage->handle(
            $this->createMock(ServerRequestInterface::class)
        );

        self::assertInstanceOf(HtmlResponse::class, $response);
    }
}
